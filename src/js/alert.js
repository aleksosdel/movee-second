const overlay = document.querySelector('.overlay'),
      btn     = document.querySelector('.main__btn'),
      close   = document.querySelector('#close')

btn.addEventListener('click', () => {
  overlay.classList.add('act')
})

close.addEventListener('click', () => {
  overlay.classList.remove('act')
})